/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewsearchsort.model;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author tomjacques
 */
public class RecordHeader {
    private static ObservableList<RecordHeader> headerList = FXCollections.observableArrayList();
    private List<Object> recordHeader = new ArrayList<>();
    
    public RecordHeader() {
        
    }
    
    public RecordHeader(List<Object> recordHeader) {
        this.recordHeader.addAll(recordHeader);
    }

    /**
     * @return the recordHeader
     */
    public List<Object> getRecordHeader() {
        return recordHeader;
    }

    /**
     * @param recordHeader the recordHeader to set
     */
    public void setRecordHeader(List<Object> recordHeader) {
        this.recordHeader = recordHeader;
    }
    
    /**
     * @return the header List
     */
    public static ObservableList<RecordHeader> getHeaderList() {
        return headerList;
    }
    
    
}
