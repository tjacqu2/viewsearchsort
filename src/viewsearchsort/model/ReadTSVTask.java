/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewsearchsort.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.BorderPane;
import viewsearchsort.ViewSearchSortMain;
import static viewsearchsort.ViewSearchSortMain.attributeCol;
import static viewsearchsort.ViewSearchSortMain.colDropDown;
import static viewsearchsort.ViewSearchSortMain.getAbsoluteFilePath;
import static viewsearchsort.ViewSearchSortMain.recordsTable;
import static viewsearchsort.ViewSearchSortMain.searchType;
import static viewsearchsort.ViewSearchSortMain.sortList;


/**
 *
 * @author tomjacques
 */
public class ReadTSVTask extends Task {
    //public static ObservableList<String> colDropDown = null;
    private File file;
    private String fileType;
    
    
    
    public ReadTSVTask(File file, String fileType) {
        this.file = file;
        this.fileType = fileType;
    }
    
    @Override
    protected Object call() throws Exception {//was public Void
        String tsvFile = getAbsoluteFilePath(file);
                String line;
                String delimiter = "\t";
                int lineCount = 0;
                int lineCountMax = 0;
                int headerLength = 0;
                int recordListSize = 0;
                int recordCount = 0;
                int recordsTossedCount = 0;
                StringBuilder rtt = new StringBuilder();
                boolean recordAlert = false;
                try (BufferedReader buffRead = new BufferedReader(new FileReader(tsvFile))) {
                    //updateProgress(.5, 1);
                    updateTitle("Initializing");
                    int firstLineLength = 0;
                    while ((line = buffRead.readLine()) != null) {
                        String[] recordLength = line.split(delimiter);
                        if (lineCount == 0) {
                            firstLineLength = recordLength.length;
                            lineCount++;
                        } else  {
                            int subsequentLineLengths = recordLength.length;
                            if (firstLineLength == subsequentLineLengths) {                                
                                lineCount++;
                            }
                            else if (subsequentLineLengths > 1 && subsequentLineLengths < firstLineLength) {
                                lineCount++;
                            }
                        }
                        
                        
                        //recordLength = null;
                        lineCountMax = lineCount;
                        
                    }
                    lineCount = 0;
                }catch (IOException e){
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error on line count");
                        alert.setContentText("Re-Try file");
                        alert.showAndWait();
                    });
                }
                
                try (BufferedReader buffRead = new BufferedReader(new FileReader(tsvFile))) {
                    int firstLineLength = 0;
                    RecordHeader.getHeaderList().clear();
                    Record.getRecordsList().clear();
                    List<Object> record = new ArrayList<>();
                    
                    //int recordCount = 0;
                    while ((line = buffRead.readLine()) != null) {
                        record.clear();//clear record for next record
                        String[] temp = line.split(delimiter);
                        for (String s : temp) {
                            String str = s.replace("\0", "");
                            record.add(str);
                            //str = null;
                        }                       
                        if (lineCount == 0) {
                            RecordHeader rh = new RecordHeader(record);//load first record to header list
                            headerLength = rh.getRecordHeader().size();
                            firstLineLength = temp.length;
                            colDropDown = FXCollections.observableArrayList(temp);
                            RecordHeader.getHeaderList().add(rh);
                            lineCount++;
                        } else {
                            int subsequentLineLengths = temp.length;
                            if (firstLineLength == subsequentLineLengths) {
                                Record r = new Record(record);//load record to record list
                                Record.getRecordsList().add(r);
                                lineCount++;
                                recordCount++;
                            }
                            //**********
                            
                            else if (subsequentLineLengths > 1 && subsequentLineLengths < firstLineLength) {
                                recordCount++;
                                recordsTossedCount++;
                                lineCount++;
                                rtt.append("Record ").append(Integer.toString(recordCount)).append(" was thrown out: Improper length").append("\n");
                                recordAlert = true;
                            }
                            //**********
                        }
                        //temp = null;
                        
                        updateProgress(lineCount, lineCountMax);
                        updateTitle("Translating file: Line " + lineCount + " of " + (lineCountMax - 1));
                        
                    }
                    
                }catch (IOException e){
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error on file translate");
                        alert.setContentText("Re-Try file");
                        alert.showAndWait();
                    });    
                }
                recordListSize = Record.getRecordsList().size();//+1 is for the header
                //set attribute types by column
                updateTitle("Converting Objects");
                //**********************************
                List<Object> record = new ArrayList<>();
                try {
                for (int col = 0; col < headerLength; col++) {//for record size, create a column
                    
                    int row = 0;
                    boolean loadAsString = false;
                    boolean isInteger = false;
                    boolean isDate = false;
                    boolean isString = false;
                    OffsetDateTime odt = null;
                    ViewSearchSortMain.runColumnAsDayFirst = false;
                    ViewSearchSortMain.isMonthFirst = false;
                    
                    for (row = 0; row < recordListSize; row++) {//for each record 

                        String attribute = (String) Record.getRecordsList().get(row).getRecord().get(col);//single attribute
                        if (loadAsString) {

                            record.add((String)attribute);
                        } else {
                            try {//is it an integer?                                
                                record.add((Integer)Integer.parseInt(attribute));
                                isInteger = true;
                                //System.out.println(attribute);
                            } catch (NumberFormatException nfe) {
                                
                                try {//is it a date
                                    //don't throw exception if not a date, speeds up process 2.5 times
                                    if (attribute.length() > 5 && (!Character.toString(attribute.charAt(2)).matches("/") || !Character.toString(attribute.charAt(5)).matches("/")) 
                                            || (attribute.length() < 6)) {
                                        //not a date
                                        record.add((String)attribute);
                                        isString = true;
                                    }
                                    else {
                                        odt = ViewSearchSortMain.convertDate(attribute, row, recordListSize);
                                        if (odt != null) {
                                            record.add(odt);
                                            isDate = true;
                                        }
                                    }
                                     
                                } catch (ParseException | DateTimeParseException e) {//must be a string
                                    record.add((String)attribute);
                                    isString = true;
                                }
                            }
                        }
                        if ((isInteger && isDate) || (isInteger && isString) || (isDate && isString)) {//if column has 2 different object types, revert to String
                           row = -1;
                           loadAsString = true;
                           isInteger = false;
                           isDate = false;
                           isString = false;
                           record.clear();
                        }
                        //marker for dd/MM
                        if (ViewSearchSortMain.isDayFirst) {
                            row = -1;
                            record.clear();
                            ViewSearchSortMain.isDayFirst = false;
                        }
                        //*****************
                        String type = "";
                        if (isInteger) {type = " to number";}
                        if (isDate) {type = " to date";}
                        if (isString) {type = " to string";}
                        updateProgress(row, recordListSize);
                        updateTitle("Converting column " + (col + 1) + " of " + headerLength + type + "\n"
                        + "Converting row " + (row + 1) + " of " + recordListSize);
                    }
                    TempRecord tr = new TempRecord(record);
                    TempRecord.getInitialMasterList().add(tr);
                    record.clear();
                    if (!loadAsString && isDate) {
                        if (ViewSearchSortMain.isMonthFirst || ViewSearchSortMain.runColumnAsDayFirst) {
                            String dateFormat = (String) RecordHeader.getHeaderList().get(0).getRecordHeader().get(col);
                            dateFormat += "(YYYY-MM-DD)";
                            RecordHeader.getHeaderList().get(0).getRecordHeader().set(col, dateFormat);
                        }
                    }
                }
                } catch(Exception e) {
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error on object type conversion");
                        alert.setContentText("Re-Try file");
                        alert.showAndWait();
                    });
                }
                //load records from master list
                
                int rowList = TempRecord.getInitialMasterList().get(0).getTempRecord().size();//c
                int columnList = TempRecord.getInitialMasterList().size();//r
                Record.getRecordsList().clear();
                int j = 0, k = 0;
                try{
                    for (k = 0; k < rowList; k++) {//c
                        for (j = 0; j < columnList; j++) {//r
                            Object tempAttribute = TempRecord.getInitialMasterList().get(j).getTempRecord().get(k);
                            record.add(tempAttribute);
                            updateTitle("Loading Converted Objects" + "\n"
                                    + "Column " + (j + 1) + " of " + columnList + "\n"//r
                                    + "Row " + (k + 1)  + " of " + rowList);//c 
                                    
                        }
                        Record r = new Record(record);
                        Record.getRecordsList().add(r);
                        record.clear();
                        updateProgress(k, rowList);
                        
                    }
                } catch (Exception e) {
                    //Alert reformat issue
                    Platform.runLater(() -> {
                        e.printStackTrace();
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error on file load");
                        alert.setContentText("Re-Try file");
                        alert.showAndWait();
                    });
                }
                TempRecord.getInitialMasterList().clear();
                //**********************************
                //**********************************
                if (recordAlert) {
                    final String recordsTossed = rtt.toString();
                    Platform.runLater(() -> {
                        BorderPane bp = new BorderPane();
                        Label lbl = new Label(recordsTossed);
                        lbl.setMinWidth(95);
                        bp.setMinSize(100, 100);
                        bp.setTop(lbl);
                        ScrollPane scroll = new ScrollPane();
                        scroll.setContent(bp);
                        Alert alert1 = new Alert(Alert.AlertType.WARNING);
                        alert1.setHeight(300);
                        alert1.getDialogPane().setContent(scroll);
                        alert1.setTitle("Alert");
                        alert1.setHeaderText("These records were omitted due to improper length:");
                        alert1.showAndWait();
                    });
                }
                //updateProgress(1, 1);                              
                lineCount = 0;
                //**********************************
                //load table
                List<String> colNames = new ArrayList<>();
                List<Object> recordData = new ArrayList<>();
                //add columns dynamically
                int count = 0;
                final int numRecords = Record.getRecordsList().size();

                //Header only
                for (RecordHeader rh : RecordHeader.getHeaderList()) {
                    colNames.clear();
                    for (Object s : rh.getRecordHeader()) {
                        colNames.add((String)s);
                    }
                    //put attribute names at top of columns
                    for (int x = 0; x < colNames.size(); x++) {
                            final int finalIndex = x;
                            TableColumn<ObservableList<Object>, Object> recordCol = new TableColumn<>(
                                    colNames.get(x)
                             
                            );
                            recordCol.setCellValueFactory(param ->
                                    new ReadOnlyObjectWrapper<>(param.getValue().get(finalIndex))
                            );
                            Platform.runLater(() -> {
                                recordsTable.getColumns().add(recordCol);
                            });
                        }
                }
                //-----------
                //Each Record
                for (Record r : Record.getRecordsList()) {
                    final int finalCount = count;
                    recordData.clear();
                    
                    for (Object s : r.getRecord()) {
                        if (s instanceof Integer) {
                            recordData.add((Integer)s);
                        }
                        if (s instanceof OffsetDateTime) {//Date object
                            recordData.add((OffsetDateTime)s);
                        }
                        if (s instanceof String) {
                            recordData.add((String)s);
                        }
                    }
                    //add rows with data dynamically
                    updateProgress(finalCount, numRecords - 1);
                    updateTitle("Table loading: Record " + count + " of " + numRecords);
                    recordsTable.getItems().add(
                             FXCollections.observableArrayList(
                                recordData
                            )
                    );
                    
                    count++;
                }
                updateTitle("Table loaded: " + (recordCount - recordsTossedCount) + " of " + (recordCount) + " records loaded");
                Platform.runLater(() -> {
                    attributeCol.setItems(colDropDown);//sets header items into drop down
                    attributeCol.getSelectionModel().selectFirst();
                    searchType.setItems(sortList);
                    searchType.getSelectionModel().selectFirst();
                    ViewSearchSortMain.viewStage.setTitle(tsvFile);
                });
                //**********************************
                //**********************************
        return null;
    }
    
}
