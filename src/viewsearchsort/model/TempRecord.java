/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewsearchsort.model;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author tomjacques
 */
public class TempRecord {
    private static ObservableList<TempRecord> initialMasterList = FXCollections.observableArrayList();
    private List<Object> initialRowList = new ArrayList<>();
    
    TempRecord() {
       
    }
    
    public TempRecord (List<Object> list) {
        this.initialRowList.addAll(list);
    }
    
    /**
     * @return the initialRowList
     */
    public List<Object> getTempRecord() {
        return initialRowList;
    }

    /**
     * @param initialRowList the initialRowList to set
     */
    public void setTempRecord(List<Object> initialRowList) {
        this.initialRowList = initialRowList;
    }

    /**
     * @return the initialMasterList
     */
    public static ObservableList<TempRecord> getInitialMasterList() {
        return initialMasterList;
    }

    /**
     * @param aInitialMasterList the initialMasterList to set
     */
    public static void setInitialMasterList(ObservableList<TempRecord> InitialMasterList) {
        initialMasterList = InitialMasterList;
    }
    
    
    
}
