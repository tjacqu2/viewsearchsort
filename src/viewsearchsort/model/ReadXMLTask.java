/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewsearchsort.model;

import java.io.File;
import java.text.ParseException;
import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.BorderPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import viewsearchsort.ViewSearchSortMain;
import static viewsearchsort.ViewSearchSortMain.attributeCol;
import static viewsearchsort.ViewSearchSortMain.colDropDown;
import static viewsearchsort.ViewSearchSortMain.getAbsoluteFilePath;
import static viewsearchsort.ViewSearchSortMain.recordsTable;
import static viewsearchsort.ViewSearchSortMain.searchType;
import static viewsearchsort.ViewSearchSortMain.sortList;

/**
 *
 * @author tomjacques
 */
public class ReadXMLTask extends Task{
    
    private File file;
    private String fileType;
    
    public ReadXMLTask (File file, String fileType) {
        this.file = file;
        this.fileType = fileType;
    }
    
    @Override
    protected Object call() throws Exception {
        int recordCount = 0;
        int recordsTossedCount = 0;
        String recordsTossedTemp = "";
        StringBuilder rtt = new StringBuilder();
        boolean recordAlert = false;
        int headerLength = 0;
        int recordListSize = 0;
        String xmlFileLoc = getAbsoluteFilePath(file);
        try {
            
            File xmlFile = new File(getAbsoluteFilePath(file)); 
            if(file.exists()){
                Record.getRecordsList().clear();
                RecordHeader.getHeaderList().clear();
                updateTitle("Initializing");
                // Create a factory
                DocumentBuilderFactory factory = 
                DocumentBuilderFactory.newInstance();
                // Use the factory to create a builder
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(xmlFile);
                // Get a list of all elements in the document
                NodeList list = doc.getElementsByTagName("*");

                Element element2 = (Element)list.item(1);//records parent tag name
                Element element1 = (Element)list.item(0);//doc tag
                Element element = null;
                NodeList children = element2.getChildNodes();//get children of this element

                List<Object> firstRec = new ArrayList<>();
                List<Object> nextRec = new ArrayList<>();
                boolean firstRecord = true;

                //Record.getRecordsList().clear();
                //RecordHeader.getHeaderList().clear();

                int firstRecordLength = 0;
                int nextRecordLength = 0;
                StringBuilder sb = new StringBuilder();
                //get first set of child tags, option 2
                for (int i=2; i<list.getLength()+1; i++) {//list + 1 to get to last record
                    updateProgress(i, list.getLength());
                    updateTitle("Reading file list: Element " + i +  " of " + list.getLength());
                    if (i != list.getLength()) {element = (Element)list.item(i);}//skip on last record
                    //for first record only
                    if (!element.getNodeName().equals(element2.getNodeName()) && i != list.getLength()) {//skip on last record                            
                        if (firstRecord) {
                            firstRec.add(element.getNodeName());
                            sb.append(element.getNodeName()).append(",");
                        }
                        nextRec.add(list.item(i).getTextContent());//                        
                        
                    } else {                   
                        firstRecordLength = firstRec.size();
                        nextRecordLength = nextRec.size();
                        recordCount++;
                        if (firstRecordLength == nextRecordLength) {
                            if (firstRecord) {
                               String[] temp = sb.toString().split(",");
                               ObservableList<String> attList = FXCollections.observableArrayList(temp);
                               colDropDown = FXCollections.observableArrayList(attList);//was temp
                               RecordHeader rh = new RecordHeader(firstRec);//puts first record in header
                               headerLength = rh.getRecordHeader().size();
                               Record record1 = new Record(nextRec);
                               RecordHeader.getHeaderList().add(rh);
                               Record.getRecordsList().add(record1);
                               nextRec.clear();
                               firstRecord = false;
                            } else {
                                if (!element.getNodeName().equals(element1.getNodeName())) {
                                   Record subsequentRecords = new Record(nextRec);
                                   Record.getRecordsList().add(subsequentRecords);
                                   recordListSize = Record.getRecordsList().size();
                                   nextRec.clear();
                                   updateProgress(recordCount, recordListSize);
                                   updateTitle("Translating file: Record " + recordCount + " of " + recordListSize);
                                }
                            }
                        } else {
                            recordsTossedCount++;//use for alert                                 
                            nextRec.clear();
                            rtt.append("Record ").append(Integer.toString(recordCount)).append(" was thrown out: Improper length").append("\n");
                            recordAlert = true;
                        }

                    }
                }
            }else{
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Alert");
                    alert.setHeaderText("File not found");
                    alert.setContentText("Re-Try");
                    alert.showAndWait();
                });    
            }
        }catch (Exception e) {
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error on xml file translate");
                alert.setContentText("Re-Try file");
                alert.showAndWait();
                updateProgress(0, 1);
            });
        }
        recordListSize = Record.getRecordsList().size();
        updateTitle("Converting Objects");
        //************************************************
        List<Object> record = new ArrayList<>();
                try {
                for (int col = 0; col < headerLength; col++) {//for record size, create a column
                    int row = 0;
                    boolean loadAsString = false;
                    boolean isInteger = false;
                    boolean isDate = false;
                    boolean isString = false;
                    OffsetDateTime odt = null;
                    ViewSearchSortMain.runColumnAsDayFirst = false;
                    ViewSearchSortMain.isMonthFirst = false;
                    
                    for (row = 0; row < recordListSize; row++) {//for each record 

                        String attribute = (String) Record.getRecordsList().get(row).getRecord().get(col);//single attribute
                        if (loadAsString) {

                            record.add((String)attribute);
                        } else {
                            try {//is it an integer?                                
                                record.add((Integer)Integer.parseInt(attribute));
                                isInteger = true;
                                //System.out.println(attribute);
                            } catch (NumberFormatException nfe) {
                                try {//is it a date
                                    //don't throw exception if not a date, speeds up process 2.5 times
                                    if (attribute.length() > 5 && (!Character.toString(attribute.charAt(2)).matches("/") || !Character.toString(attribute.charAt(5)).matches("/")) 
                                            || (attribute.length() < 6)) {
                                        //not a date
                                        record.add((String)attribute);
                                        isString = true;
                                    }
                                    else {
                                        odt = ViewSearchSortMain.convertDate(attribute, row, recordListSize);
                                        if (odt != null) {
                                            record.add(odt);
                                            isDate = true;
                                        }
                                    }
                                     
                                } catch (ParseException | DateTimeParseException p) {//must be a string
                                    record.add((String)attribute);
                                    isString = true;
                                }
                            }
                        }
                        if ((isInteger && isDate) || (isInteger && isString) || (isDate && isString)) {//if column has 2 different object types, revert to String
                           row = -1;
                           loadAsString = true;
                           isInteger = false;
                           isDate = false;
                           isString = false;
                           record.clear();
                        }
                        //marker for dd/MM
                        if (ViewSearchSortMain.isDayFirst) {
                            row = -1;
                            record.clear();
                            ViewSearchSortMain.isDayFirst = false;
                        }
                        //*****************
                        String type = "";
                        if (isInteger) {type = " to number";}
                        if (isDate) {type = " to date";}
                        if (isString) {type = " to string";}
                        updateProgress(row, recordListSize);
                        updateTitle("Converting column " + (col + 1) + " of " + headerLength + type + "\n"
                        + "Converting row " + (row + 1) + " of " + recordListSize);
                    }
                    TempRecord tr = new TempRecord(record);
                    TempRecord.getInitialMasterList().add(tr);
                    record.clear();
                    if (!loadAsString && isDate) {
                        if (ViewSearchSortMain.isMonthFirst || ViewSearchSortMain.runColumnAsDayFirst) {
                            String dateFormat = (String) RecordHeader.getHeaderList().get(0).getRecordHeader().get(col);
                            dateFormat += "(YYYY-MM-DD)";
                            RecordHeader.getHeaderList().get(0).getRecordHeader().set(col, dateFormat);
                        }
                    }
                }
                } catch(Exception e) {
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error on object type conversion");
                        alert.setContentText("Re-Try file");
                        alert.showAndWait();
                    });
                }
                //load records from master list
                int rowList = TempRecord.getInitialMasterList().get(0).getTempRecord().size();
                int columnList = TempRecord.getInitialMasterList().size();
                Record.getRecordsList().clear();
                int j = 0, k = 0;
                try{
                    for (k = 0; k < rowList; k++) {
                        for (j = 0; j < columnList; j++) {
                            Object tempAttribute = TempRecord.getInitialMasterList().get(j).getTempRecord().get(k);
                            record.add(tempAttribute);
                            updateTitle("Loading Converted Objects" + "\n"
                                    + "Column " + (j + 1) + " of " + columnList + "\n"//r
                                    + "Row " + (k + 1)  + " of " + rowList);//c 
                        }
                        Record r = new Record(record);
                        Record.getRecordsList().add(r);
                        record.clear();
                        updateProgress(k, rowList);
                        //updateProgress(k, columnList);
                    }
                } catch (Exception e) {
                    //Alert reformat issue
                    Platform.runLater(() -> {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error on file load");
                        alert.setContentText("Re-Try file");
                        alert.showAndWait();
                    });
                }
                TempRecord.getInitialMasterList().clear();
                //**********************************
                //**********************************
                //updateProgress(1, 1);                              
                //lineCount = 0;
        //************************************************
        //************************************************
        if (recordAlert) {
            final String recordsTossed = rtt.toString();
            Platform.runLater(() -> {
                BorderPane bp = new BorderPane();
                        Label lbl = new Label(recordsTossed);
                        lbl.setMinWidth(95);
                        bp.setMinSize(100, 100);
                        bp.setTop(lbl);
                        ScrollPane scroll = new ScrollPane();
                        scroll.setContent(bp);
                        Alert alert1 = new Alert(Alert.AlertType.WARNING);
                        alert1.setHeight(300);
                        alert1.getDialogPane().setContent(scroll);
                        alert1.setTitle("Alert");
                        alert1.setHeaderText("These records were omitted due to improper length:");
                        alert1.showAndWait();
                        
            });
            rtt = null;
        }
        //***************************************
        //***************************************
        //load table
        List<String> colNames = new ArrayList<>();
        List<Object> recordData = new ArrayList<>();
        //add columns dynamically
        int count = 0;
        final int numRecords = Record.getRecordsList().size();

        //Header only
        for (RecordHeader rh : RecordHeader.getHeaderList()) {
            colNames.clear();
            for (Object s : rh.getRecordHeader()) {
                colNames.add((String)s);
            }
            //put attribute names at top of columns
            for (int x = 0; x < colNames.size(); x++) {
                    final int finalIndex = x;
                    TableColumn<ObservableList<Object>, Object> recordCol = new TableColumn<>(
                            colNames.get(x)

                    );
                    recordCol.setCellValueFactory(param ->
                            new ReadOnlyObjectWrapper<>(param.getValue().get(finalIndex))
                    );
                    Platform.runLater(() -> {
                        recordsTable.getColumns().add(recordCol);
                    });
                }
        }
        //-----------
        //Each Record
        for (Record r : Record.getRecordsList()) {
            final int finalCount = count;
            recordData.clear();

            for (Object s : r.getRecord()) {
                if (s instanceof Integer) {
                    recordData.add((Integer)s);
                }
                if (s instanceof OffsetDateTime) {
                    recordData.add((OffsetDateTime)s);
                }
                if (s instanceof String) {
                    recordData.add((String)s);
                }
            }
            //add rows with data dynamically
            updateProgress(finalCount, numRecords - 1);
            updateTitle("Table loading: Record " + count + " of " + numRecords);
            recordsTable.getItems().add(
                     FXCollections.observableArrayList(
                        recordData
                    )
            );
           
            count++;
        }
        updateTitle("Table loaded: " + (recordCount - recordsTossedCount) + " of " + (recordCount) + " records loaded");
        Platform.runLater(() -> {
            attributeCol.setItems(colDropDown);//sets header items into drop down
            attributeCol.getSelectionModel().selectFirst();
            searchType.setItems(sortList);
            searchType.getSelectionModel().selectFirst();
            ViewSearchSortMain.viewStage.setTitle(xmlFileLoc);
        });
    return null;
    }
}
