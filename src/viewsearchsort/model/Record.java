/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewsearchsort.model;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author tomjacques
 */
public class Record {
    
    private static ObservableList<Record> recordsList = FXCollections.observableArrayList();
    private List<Object> record = new ArrayList<>();
    
    public Record() {
        
    }
           
    public Record(List<Object> record) {
        this.record.addAll(record);
        
    }
    /**
     * @return the record List
     */
    public static ObservableList<Record> getRecordsList() {
        return recordsList;
    }

    /**
     * @return the record
     */
    public List<Object> getRecord() {
        return record;
    }

    /**
     * @param record the record to set
     */
    public void setRecord(List<Object> record) {
        this.record = record;
    }
}
