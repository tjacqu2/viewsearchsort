/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewsearchsort.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.concurrent.Future;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import static viewsearchsort.ViewSearchSortMain.getAbsoluteFilePath;


/**
 *
 * @author tomjacques
 */
public class TSVtoXMLTask extends Task{
    
    private File file;
    private String fileType;
    public static Writer output;
    public static boolean save = true;
    
    public TSVtoXMLTask(File file, String fileType) {
        this.file = file;
        this.fileType = fileType;
    }
    
    @Override
    protected Object call() throws Exception {
        save = true;
        String recordsTossedTemp = "";
        ArrayList<String> al = new ArrayList<>();
        String tsvFile = getAbsoluteFilePath(file);
        String xmlFile = "";
        String line = "";
        
        String delimiter = "\t";
        int firstRecordLength = 0;
        int nextRecordLength = 0;
        boolean alert = false;
            updateTitle("Initializing");
            try {
            DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = df.newDocumentBuilder();
            Document document = db.newDocument();
            Element rootElement = document.createElement("MediaRecords");
            int lineFile = 0;
            int totalLines = 0;
            document.appendChild(rootElement);
            try (BufferedReader buffRead = new BufferedReader(new FileReader(tsvFile))) {
                while ((line = buffRead.readLine()) != null) {
                    lineFile++;
                    totalLines = lineFile;
                    //System.out.println(totalLines);
                }
                lineFile = 0;
            } catch(IOException e) {
                e.printStackTrace();
            }       
                try (BufferedReader buffRead2 = new BufferedReader(new FileReader(tsvFile))) {
                while ((line = buffRead2.readLine()) != null) {
                    String[] record = line.split(delimiter);
                    nextRecordLength = record.length;
                    if (lineFile == 0) {
                        firstRecordLength = record.length;
                        for (String s : record) {
                            String attributeInfo = s.replace("\0", "");
                            al.add(attributeInfo);
                        }
                    }else{
                        if (firstRecordLength == nextRecordLength) { 
                            Element childElement = document.createElement("recordDetails");
                            rootElement.appendChild(childElement);

                            for (int recordInfo = 0; recordInfo < al.size(); recordInfo++) {

                                String header = al.get(recordInfo);
                                String value = null;

                                if (recordInfo < record.length) {
                                    value = record[recordInfo];
                                } else {
                                    value = " ";
                                }

                                Element current = document.createElement(header);
                                current.appendChild(document.createTextNode(value));
                                childElement.appendChild(current);
                            }
                        } else {
                            if (nextRecordLength == 1) {
                            }
                            else if (nextRecordLength > 1 && nextRecordLength < firstRecordLength) {
                            //get number of record tossed and add to String
                            recordsTossedTemp += "Record " + Integer.toString(lineFile) + " was thrown out: Improper length" + "\n";
                            alert = true;
                            }
                        }
                    }
                    lineFile++;
                    System.out.println(lineFile);
                    updateProgress(lineFile, totalLines - 1);
                    updateTitle("Translating file");
                }
                updateTitle("Converting file");
                updateProgress(.5, 1);
                Transformer tf = TransformerFactory.newInstance().newTransformer();
                tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tf.setOutputProperty(OutputKeys.INDENT, "yes");
                output = new StringWriter();
                tf.transform(new DOMSource(document), new StreamResult(output));
                
            }catch (IOException e) {
                e.printStackTrace();
            }

        }catch (Exception e) {
            save = false;
            Platform.runLater(() -> {
            Alert alert1 = new Alert(Alert.AlertType.ERROR);
            alert1.setTitle("Error");
            alert1.setHeaderText("Illegal XML character");
            alert1.setContentText("File not translatable");
            alert1.showAndWait();
            
            });
        }
        if (alert) {
            final String recordsTossed = recordsTossedTemp;
            Platform.runLater(() -> {
            Alert alert1 = new Alert(Alert.AlertType.WARNING);
            alert1.setTitle("Alert");
            alert1.setHeaderText("These records were omitted due to improper length:");
            alert1.setContentText(recordsTossed);
            alert1.showAndWait();
            //bring focus back to save stage
            });
        }
        
        if (save) {
            updateProgress(1, 1);
            updateTitle("Conversion complete");
        } else {
            updateProgress(0, 1);
            updateTitle("Conversion fail");
        }
        
        //readyToSave = true;
        return 1;
    }
    
}
