/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewsearchsort.controller;

import java.io.File;
import java.net.URL;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import viewsearchsort.ViewSearchSortMain;
import static viewsearchsort.ViewSearchSortMain.attributeCol;
import static viewsearchsort.ViewSearchSortMain.colDropDown;
import static viewsearchsort.ViewSearchSortMain.recordsTable;
import static viewsearchsort.ViewSearchSortMain.searchFieldText;
import viewsearchsort.model.Record;
import static viewsearchsort.ViewSearchSortMain.searchType;
import static viewsearchsort.ViewSearchSortMain.spacerHoriz;
import viewsearchsort.model.ReadTSVTask;
//import viewsearchsort.model.ReadXMLTask;
 
/**
 * FXML Controller class
 *
 * @author tomjacques
 */
public class MainScreenFXMLController implements Initializable {
    @FXML
    BorderPane paneTableView;
    
    private ViewSearchSortMain mainApp;
    private static int choiceBoxColumnIndex;
    private static int choiceBoxSearchIndex;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
            searchFieldText.textProperty().addListener((observable, oldValue, newValue) -> handleSearchAction(choiceBoxColumnIndex));
            attributeCol.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    choiceBoxColumnIndex = newValue.intValue();
                    searchFieldText.clear();
                }
            });
            searchType.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    choiceBoxSearchIndex = newValue.intValue();
                    searchFieldText.clear();
                    if (choiceBoxSearchIndex == 0) {searchFieldText.setPromptText("Search starts with");}
                    else {searchFieldText.setPromptText("Search contains");}
                }
            });
            paneTableView.widthProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                    ViewSearchSortMain.paneWidth = (double) newValue;
                    spacerHoriz.setMinSize(100 + ViewSearchSortMain.paneWidth - 600, 10);
                }
                
            });
            
    }    
    
    private void handleSearchAction(int ColumnNum) {
        
        String searchText = searchFieldText.getText();
        FilteredList<Record> searchResults = searchRecords(searchText, ColumnNum, choiceBoxSearchIndex);
        SortedList<Record> sortedData = new SortedList<>(searchResults);
        loadTableFilteredList(sortedData);
        if (searchResults.isEmpty() && !searchFieldText.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Alert");
            alert.setHeaderText("No Matching Records");
            alert.setContentText("No matching records found. Re-try search");
            alert.showAndWait();
            searchFieldText.setText(searchFieldText.getText().substring(0, searchFieldText.getText().length() -1));
        }
    }
    
    private FilteredList<Record> searchRecords(String s, int index, int searchType){
        if (searchType == 0) {            
            return Record.getRecordsList().filtered(r -> r.getRecord().get(index).toString().toLowerCase().startsWith(s.toLowerCase())); 
        } else {
            return Record.getRecordsList().filtered(r -> r.getRecord().get(index).toString().toLowerCase().contains(s.toLowerCase())); 
        }
    }
        
    
    public void loadTableFilteredList(SortedList<Record> sortedRecords) {
        List<Object> recordData = new ArrayList<>();
        recordsTable.getItems().clear();
        for (Record r : sortedRecords) {
            recordData.clear();
            for (Object s : r.getRecord()) {
                        if (s instanceof Integer) {
                            recordData.add((Integer)s);
                        }
                        if (s instanceof OffsetDateTime) {
                            recordData.add((OffsetDateTime)s);
                        }
                        if (s instanceof String) {
                            recordData.add((String)s);
                        }
                    }
            //add rows with data dynamically
            recordsTable.getItems().add(
                     FXCollections.observableArrayList(
                        recordData
                    )
            );
        }
    }
    
    
    
    public void handleTSVFileBrowse(ActionEvent event) {
        Record.getRecordsList().clear();
        ExecutorService ex = Executors.newSingleThreadExecutor();//for running tasks consecutively
        //Stage stage = new Stage();
        //stage.setTitle("Browse");
        
        final FileChooser fileChooser = new FileChooser();
        
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("TSV Files", "*.tsv")
                );
        File file = fileChooser.showOpenDialog(null);
        
        if (file != null) {
            fileChooser.setInitialDirectory(file.getParentFile());
            ViewSearchSortMain.viewStage.setTitle("Records");
            //stage.close();
            recordsTable.getItems().clear();
            recordsTable.getColumns().clear();
            if (colDropDown != null) {colDropDown.clear();}
            if (attributeCol != null) {attributeCol.getItems().clear();}
            ReadTSVTask task1 = new ReadTSVTask(file, "tsv");
            //Thread th = new Thread(task1);
            //th.setDaemon(true);
            //th.start();
            ex.submit(task1);//for running tasks consecutively
            ViewSearchSortMain.pb.progressProperty().bind(task1.progressProperty());
            ViewSearchSortMain.lbl1.textProperty().bind(task1.titleProperty());
            ex.shutdown();//for running tasks consecutively
        }
        
    }
    /*@FXML
    public void handleXMLFileBrowse(ActionEvent event) {
        Record.getRecordsList().clear();
        ExecutorService ex = Executors.newSingleThreadExecutor();
        Stage stage = new Stage();
        stage.setTitle("Browse");
        
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("XML Files", "*.xml")
                );
        File file = fileChooser.showOpenDialog(stage);
        
        if (file != null) {
            stage.close();
            recordsTable.getItems().clear();
            recordsTable.getColumns().clear();
            if (colDropDown != null) {colDropDown.clear();}
            if (attributeCol != null) {attributeCol.getItems().clear();}
            ReadXMLTask task2 = new ReadXMLTask(file, "xml");
            ex.submit(task2);
            ViewSearchSortMain.pb.progressProperty().bind(task2.progressProperty());
            ViewSearchSortMain.lbl1.textProperty().bind(task2.titleProperty());
            ex.shutdown();
        }
    }*/
    
    /*@FXML
    public void handleTSVtoXMLConvert(ActionEvent event) {
        ExecutorService ex = Executors.newSingleThreadExecutor();
        Future<Object> result = null;
        Stage stage = new Stage();
        stage.setTitle("Browse");
        Stage fileSave = new Stage();
        fileSave.initOwner(viewStage);//viewStage needs to be static for this
        fileSave.setTitle("Save xml as");
        
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("TSV Files", "*.tsv")
                );
        File file = fileChooser.showOpenDialog(stage);
        
        if (file != null) {
            stage.close();
            recordsTable.getItems().clear();
            recordsTable.getColumns().clear();
            if (colDropDown != null) {colDropDown.clear();}
            if (attributeCol != null) {attributeCol.getItems().clear();}
            TSVtoXMLTask task3 = new TSVtoXMLTask(file, "tsv");
            //ex.submit(task3);
            result = (Future<Object>) ex.submit(task3);
            ViewSearchSortMain.pb.progressProperty().bind(task3.progressProperty());
            ViewSearchSortMain.lbl1.textProperty().bind(task3.titleProperty());
            FileChooser fcSave = new FileChooser();
            fcSave.setInitialDirectory(
            new File(System.getProperty("user.home"))
            );
            fcSave.setInitialFileName("newXML.xml");
            fcSave.getExtensionFilters().addAll(
                new ExtensionFilter("XML Format", "*.xml")
                );
            
            try {
                result.get();
            } catch (InterruptedException ex1) {
                Logger.getLogger(MainScreenFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (ExecutionException ex1) {
                Logger.getLogger(MainScreenFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
            }
            ex.shutdown();
            
            if (save) { 
                fileSave.initModality(Modality.APPLICATION_MODAL);
                File file2 = fcSave.showSaveDialog(viewStage);
                if (file2 != null) {  
                    String xmlFile = getAbsoluteFilePath(file2);  
                    PrintWriter writer ;
                    try {

                        writer = new PrintWriter(xmlFile, "UTF-8");
                        writer.println(output.toString());
                        writer.close();

                    } catch (FileNotFoundException | UnsupportedEncodingException ex1) {
                        Logger.getLogger(MainScreenFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                }    
            }
        }
    }*/
    
        
    @FXML
    public void handleSystemExit(ActionEvent event) {
        System.exit(0);
    }

    public void setMainApp(ViewSearchSortMain mainApp) {
        this.mainApp = mainApp;
    }
    
}
