/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viewsearchsort;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import viewsearchsort.controller.MainScreenFXMLController;

/**
 *
 * @author tomjacques
 */
public class ViewSearchSortMain extends Application{
    
    public static ObservableList<String> colDropDown = null;
    public static Stage viewStage;
    private AnchorPane rootLayout;
    //private final Desktop desktop = Desktop.getDesktop();
    public static ProgressBar pb = new ProgressBar();
    public static ChoiceBox<String> attributeCol = new ChoiceBox<>();
    public static Label lbl1 = new Label("Progress");
    public static ObservableList<String> sortList = FXCollections.observableArrayList("Search starts with", "Search contains");
    public static ChoiceBox<String> searchType = new ChoiceBox<>();
    public static TextField searchFieldText = new TextField();
    public static boolean init = false;
    public static TableView<ObservableList<Object>> recordsTable;
    public static double paneWidth;
    public static Pane spacerHoriz = new Pane();
    public static double rlWidth = 800;
    public static double rlHeight = 600;
    public static boolean runColumnAsDayFirst = false;
    public static boolean isDayFirst = false;
    public static boolean isMonthFirst = false;
    
    
    @Override
    public void start(Stage primaryStage) throws Exception { 
        this.viewStage = primaryStage;
        this.viewStage.setTitle("Records");
        rootLayout = new AnchorPane();
        initRootLayout();
        showMainLayout();
    }
    
    private void initRootLayout() {
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ViewSearchSortMain.class.getResource("controller/RootLayoutFXML.fxml"));
            rootLayout = (AnchorPane) loader.load();
            Scene scene = new Scene(rootLayout,800,600);
            viewStage.setScene(scene);
            scene.getStylesheets().add("vssStyleSheet.css");
            viewStage.show();
        }catch(Exception e) {
            //e.printStackTrace();
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error on program load");
            alert.setContentText("Re-Try");
            alert.showAndWait();
        }
    }
    
    /*private void showLogIn() {
        //setup stage and scene for login screen
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ViewSearchSortMain.class.getResource("controller/LogInFXML.fxml"));    
            BorderPane logIn = (BorderPane) loader.load();
            Scene scene = new Scene(logIn);
            viewStage.setScene(scene);
            viewStage.setTitle("Log In");
            viewStage.show();
            LogInFXMLController controller = loader.getController();
            controller.setMainApp(this);
        }catch(Exception e){
           e.printStackTrace(); 
        }
    }*/
    
    public void showMainLayout() throws IOException{
        init = true;
        pb.setProgress(0);
        //Button btn1 = new Button("tsv to xml");
        Button btn4 = new Button("Choose TSV File");
        //Button btn5 = new Button("Choose XML File");
        Button btn3 = new Button("Exit");
        
        Pane spacerVert = new Pane();
        spacerHoriz.setMinSize(100, 10);//140
        spacerVert.setMinSize(10, 33);
        //VBox vb = new VBox(btn4, btn5, btn1);
        //VBox vb = new VBox(btn4, btn5);
        VBox vb = new VBox(btn4);
        vb.setSpacing(2);
        vb.setPrefWidth(115);
        //btn1.setMinWidth(vb.getPrefWidth());
        btn4.setMinWidth(vb.getPrefWidth());
        //btn5.setMinWidth(vb.getPrefWidth());
        VBox vb2 = new VBox(pb, lbl1);
        vb2.setPadding(new Insets(0,0,0,10));
        VBox vb3 = new VBox(spacerHoriz);
        VBox vb4 = new VBox(spacerVert, btn3);
        HBox hb = new HBox(vb, vb2, vb3, vb4);
        hb.setSpacing(2);
        pb.setPrefWidth(330);
        HBox hb2 = new HBox(searchFieldText, searchType, attributeCol);
        hb2.setSpacing(2);
        searchFieldText.setPromptText("Search starts with");
        attributeCol.setTooltip(new Tooltip("Select Column for Search"));
        searchType.setTooltip(new Tooltip("Select search type"));
        recordsTable = new TableView<>();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(ViewSearchSortMain.class.getResource("controller/MainScreenFXML.fxml"));
        BorderPane mainView = (BorderPane) loader.load();
        mainView.setCenter(recordsTable);
        mainView.setBottom(hb);
        mainView.setTop(hb2);
        BorderPane.setMargin(hb2, new Insets(3));
        BorderPane.setMargin(hb, new Insets(3));
        rootLayout.getChildren().add(mainView);
        rootLayout.setTopAnchor(mainView, (double) 25);
        rootLayout.setBottomAnchor(mainView, (double) 25);
        rootLayout.setLeftAnchor(mainView, (double) 25);
        rootLayout.setRightAnchor(mainView, (double) 25);
        rootLayout.getStyleClass().add("paneBackground");
        MainScreenFXMLController controller = loader.getController();
        btn4.setOnAction(controller::handleTSVFileBrowse);
        //btn5.setOnAction(controller::handleXMLFileBrowse);
        //btn1.setOnAction(controller::handleTSVtoXMLConvert);
        btn3.setOnAction(controller::handleSystemExit);
        controller.setMainApp(this);
    }
    
    public static String getAbsoluteFilePath(File file) {
        String absolutePath = file.getAbsolutePath();
        return absolutePath;
    }
    
    public static OffsetDateTime convertDate(String dateAttribute, int rowCount, int rowLength) throws ParseException, DateTimeParseException{
        boolean isValid = false;
        OffsetDateTime dateUTC = null;
        Date date = null;
        String attributeSub = "";
        String lastChars = "";
        boolean isShortDate = false;
        if (dateAttribute.length() > 10) {
            if (Character.toString(dateAttribute.charAt(8)).matches(" ")) {
                attributeSub = dateAttribute.substring(0, 8);
                isShortDate = true;
            }
            else if (Character.toString(dateAttribute.charAt(10)).matches(" ")) {
                attributeSub = dateAttribute.substring(0, 10);
                isShortDate = false;
            }
        }
        List<String> dateFormats; 
            dateFormats = new ArrayList<String>() {{
                
                add("MM/dd/yy");
                add("dd/MM/yy");
                add("MM/dd/yyyy");// kk:mm:ss
                add("000000");//throw exception, leave as string
            }
        };
        for (int j = 0; j < dateFormats.size(); j++) {
            //marker for dd/MM
            if (runColumnAsDayFirst) {
                j++;
            }
            //**********************
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormats.get(j));//date format to use for parsing
            date = sdf.parse(attributeSub);//see if the string will parse to the above format, if not, throw parse exception
            String dateCheck = sdf.format(date);//convert date into a string with the new format
            if (attributeSub.equals(dateCheck)) {//&& the format of dateFormats.get(j) matches format marker
                //marker for dd/MM
                if (!runColumnAsDayFirst) {
                    if (Integer.parseInt(attributeSub.substring(0, 2)) > 12) {
                       isDayFirst = true;
                       runColumnAsDayFirst = true;
                    }
                    if (Integer.parseInt(attributeSub.substring(3, 5)) > 13 && !isDayFirst) {//is month/day for sure?
                        isMonthFirst = true;
                    }
                } 
                //***************
                sdf = new SimpleDateFormat("yyyy-MM-dd");//create new format
                dateCheck = sdf.format(date);//convert date into a string with the new format
                if (isShortDate) {dateCheck += dateAttribute.substring(8);}
                else {dateCheck += dateAttribute.substring(10);}
                String noSpaces = dateCheck.replace(" ", "");
                //if AM/PM, convert to military time for UTC formatting
                if (noSpaces.length() > 18) {
                    if (Character.toString(noSpaces.charAt(18)).matches("A") || Character.toString(noSpaces.charAt(18)).matches("P")
                            || Character.toString(noSpaces.charAt(17)).matches("A") || Character.toString(noSpaces.charAt(17)).matches("P")) {
                        if (Character.toString(noSpaces.charAt(17)).matches("A") || Character.toString(noSpaces.charAt(17)).matches("P")) {
                            noSpaces = new StringBuilder(noSpaces).insert(10, "0").toString();
                        }
                        noSpaces = standardToMilitaryTime(noSpaces, 18);   
                    }
                }
                //insert "T" between date and time for UTC formatting
                noSpaces = new StringBuilder(noSpaces).insert(10, "T").toString();
                //determine offset for UTC formatting
                String lastCharM2 = Character.toString(noSpaces.charAt(noSpaces.length() - 2));
                String lastCharM3 = Character.toString(noSpaces.charAt(noSpaces.length() - 3));
                String lastCharM6 = Character.toString(noSpaces.charAt(noSpaces.length() - 6));                
                if (lastCharM2.equals("+") || lastCharM2.equals("-")) {
                    lastChars = noSpaces.substring(noSpaces.length() - 1);
                    if (Integer.parseInt(lastChars) == 0) {
                        noSpaces = new StringBuilder(noSpaces).append("0:00").toString();
                    }
                    else if (Integer.parseInt(lastChars) > 0 && Integer.parseInt(lastChars) < 10) {
                        noSpaces = noSpaces.substring(0, noSpaces.length() - 1);
                        noSpaces = new StringBuilder(noSpaces).append("0").append(lastChars).append(":00").toString();
                    }
                }
                else if (lastCharM3.equals("+") || lastCharM3.equals("-")) {
                    lastChars = noSpaces.substring(noSpaces.length() - 2);
                    if (Integer.parseInt(lastChars) >= 10) {
                        noSpaces = new StringBuilder(noSpaces).append(":00").toString();
                    }
                }
                else if (lastCharM6.equals("+") || lastCharM6.equals("-")) {
                    lastChars = noSpaces.substring(noSpaces.length() - 5);
                    //do nothing, UTC is good
                }
                else {noSpaces = new StringBuilder(noSpaces).append("+00:00").toString();}                

                dateUTC = OffsetDateTime.parse(noSpaces, DateTimeFormatter.ISO_OFFSET_DATE_TIME);//if not parseable, skips to catch and becomes string
                if (rowCount == rowLength - 1 && !runColumnAsDayFirst && !isMonthFirst) {
 
                    OffsetDateTime.parse("");//throw parse exception
                }
                isValid = true;
                break;
            } else {
                isValid = false;
            }
        }
        if (isValid) {                             
        return dateUTC;
        } else {
        return null;
        }
    }
    
    public static String standardToMilitaryTime(String time, int pos) {
        String militaryTime = "";
        String convert = "";
        int hour = Integer.parseInt(time.substring(10,12));
        if(Character.toString(time.charAt(pos)).matches("P")) {//char at 18 PM
            if(hour >= 1 && hour < 12){
                hour += 12;
                convert = Integer.toString(hour);
            }else{
                convert = Integer.toString(hour);
            }
        }else{// char at 18 AM
            if(hour == 12) {
                hour = 0;
                convert = "0" + Integer.toString(hour);
            }
            else if(hour >= 1 && hour <= 9){
                convert = "0" + Integer.toString(hour);
            }else{
                convert = Integer.toString(hour);
            }
        }
        militaryTime = time.substring(0, 10) + convert + time.substring(12, 18) + time.substring(20);
        return militaryTime;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
